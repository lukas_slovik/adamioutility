﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CaptureService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace CaptureService.Tests
{
    [TestClass()]
    public class CaptureTestTests
    {
        [TestMethod()]
        public void StartTestTest()
        {
            CaptureTest testFrame = new CaptureTest();
            testFrame.StartTest();
            int i = 0;
            while (i < 20)
            {
                Thread.Sleep(1000);
                i++;
            }
            i = 0;
            //testFrame.MakeSnapShot
            testFrame.Stop();
            testFrame.SaveFrames();
            while (i < 1000)
            {
                //if (testFrame.SnapShotTaken)
                //{
                //    File.AppendAllText(@"C:\Users\shaggy\Desktop\frameStart.txt", "Snapshot started " + DateTime.Now.ToString() + " " + DateTime.Now.Millisecond + Environment.NewLine);
                //    testFrame.MakeSnapShot();
                //    Thread.Sleep(100);
                //    testFrame.MakeSnapShot();
                //    i++;
                //}
                i++;
                Thread.Sleep(100);
            }
            testFrame.SaveFrames();
            Assert.Fail();
        }
    }
}