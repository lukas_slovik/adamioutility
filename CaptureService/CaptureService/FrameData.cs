﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaptureService
{
    public class FrameData
    {
        public Bitmap Frame { get; set; }
        public DateTime ReadFrame { get; set; }
        public FrameData(Bitmap frame, DateTime readFrame)
        {
            Frame = frame;
            ReadFrame = readFrame;
        }
    }
}
