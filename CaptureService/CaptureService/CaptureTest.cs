﻿using AForge.Controls;
using AForge.Video;
using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaptureService
{
    public class CaptureTest
    {
        private VideoCapabilities[] videoCapabilities;
        private VideoCapabilities[] snapshotCapabilities;
        private VideoCaptureDevice videoDevice;
        private VideoSourcePlayer videoSourcePlayer;
        public List<FrameData> Frames;
        private int counter;
        public void SaveFrames()
        {
            int index = 0;
            foreach (FrameData frame in Frames)
            {
                frame.Frame.Save(@"C:\Users\shaggy\Desktop\Frames\frame" + index + ".png", System.Drawing.Imaging.ImageFormat.Png);
                File.AppendAllText(@"C:\Users\shaggy\Desktop\frameRate.txt", "Snapshot finished " + frame.ReadFrame.ToString() + " " + frame.ReadFrame.Millisecond + Environment.NewLine);
                index++;
            }
        }

        public bool SnapShotTaken { get; set; }
        public CaptureTest()
        {
            this.counter = 0;
            this.videoSourcePlayer = new VideoSourcePlayer();
            this.Frames = new List<FrameData>();
            SnapShotTaken = true;
        }

        private void EnumeratedSupportedFrameSizes(VideoCaptureDevice videoDevice)
        {
            try
            {
                videoCapabilities = videoDevice.VideoCapabilities;
                snapshotCapabilities = videoDevice.SnapshotCapabilities;
            }
            finally
            {
            }
        }

        private void videoDevice_SnapshotFrame(object sender, NewFrameEventArgs eventArgs)
        {
            ShowSnapshot((Bitmap)eventArgs.Frame.Clone());
        }

        private void ShowSnapshot(Bitmap snapshot)
        {
            //this.Frames.Add(snapshot);
            SnapShotTaken = true;
            File.AppendAllText(@"C:\Users\shaggy\Desktop\frameEnd.txt", "Snapshot finished " + DateTime.Now.ToString() + " " + DateTime.Now.Millisecond + Environment.NewLine);
            File.AppendAllText(@"C:\Users\shaggy\Desktop\frameStart.txt", "Snapshot started " + DateTime.Now.ToString() + " " + DateTime.Now.Millisecond + Environment.NewLine);
            if (this.counter < 10)
                MakeSnapShot();
            //Console.WriteLine("test data");
            //snapshot.Save(@"C:\Users\shaggy\Desktop\frame.png", System.Drawing.Imaging.ImageFormat.Png);
        }

        private void NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            this.Frames.Add(new FrameData((Bitmap)eventArgs.Frame.Clone(), DateTime.Now));
        }

        public void StartTest()
        {
            FilterInfoCollection videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (videoDevices.Count != 0)
            {
                videoDevice = new VideoCaptureDevice(videoDevices[0].MonikerString);
                if (videoDevice != null)
                {
                    EnumeratedSupportedFrameSizes(videoDevice);
                    if ((videoCapabilities != null) && (videoCapabilities.Length != 0))
                    {
                        videoDevice.VideoResolution = videoCapabilities[8];
                        videoDevice.NewFrame += new NewFrameEventHandler(NewFrame);
                    }

                    //if ((snapshotCapabilities != null) && (snapshotCapabilities.Length != 0))
                    //{
                    //    videoDevice.ProvideSnapshots = true;
                    //    videoDevice.SnapshotResolution = snapshotCapabilities[0];
                    //    videoDevice.SnapshotFrame += new NewFrameEventHandler(videoDevice_SnapshotFrame);
                    //}
                    //videoSourcePlayer.VideoSource = videoDevice;
                    //videoSourcePlayer.Start();
                    videoDevice.Start();
                }
            }
        }

        public void MakeSnapShot()
        {
            this.counter++;
            if ((videoDevice != null) && (videoDevice.ProvideSnapshots))
            {
                SnapShotTaken = false;
                videoDevice.SimulateTrigger();
            }
        }

        public void Stop()
        {
            videoDevice.Stop();
        }
    }
}
