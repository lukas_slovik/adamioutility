﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AdamUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace AdamUtility.Tests
{
    [TestClass()]
    public class AdamConnectorTests
    {
        private bool SaveData(AdamData davaValues)
        {
            return true;
        }

        private void ErrorLog(string errorMsg)
        {
            Assert.Fail();
        }
        [TestMethod()]
        public void StartTest()
        {
            AdamConnector adamTest = new AdamConnector("10.161.156.147", 502, 10000, 1000, SaveData, ErrorLog, AdamDataReadout.ReadCounters);
            adamTest.Start();
            while (true)
            {
                Thread.Sleep(10000);
            }
        }

        [TestMethod()]
        public void StopTest()
        {
            AdamConnector adamTest = new AdamConnector("10.161.156.147", 502, 10000, 1000, SaveData, ErrorLog, AdamDataReadout.ReadCounters);
            adamTest.Start();
            bool shouldStop = true; 
            while (shouldStop)
            {
                Thread.Sleep(10000);
                shouldStop = false;
            }
            adamTest.Start();
            shouldStop = true;
            while (shouldStop)
            {
                Thread.Sleep(10000);
                shouldStop = false;
            }
        }
    }
}