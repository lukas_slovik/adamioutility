﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamUtility
{
    public enum AdamDataReadout
    {
        None = 1,
        ReadCounters = 2,
        ReadDiValues = 4,
        ReadDoValues = 8
    }
}
