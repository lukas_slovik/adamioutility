﻿using Advantech.Adam;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AdamUtility
{
    public class AdamConnector
    {
        private const int DEFALUT_TIMOUT_VALUE = 10000;
        private AdamSocket adamModbus;
        private object adamModbusLock;
        public string IpAddress { get; private set; }
        public int Port { get; private set; }
        public int ConnectTimeOut { get; set; }
        public int SendTimeOut { get; set; }
        public int ReceiveTimeOut { get; set; }
        public int DisconnectTimeOut { get; set; }
        public int ReadTimeOut { get; set; }
        public int StopSleepTimeOut { get; set; }
        public delegate bool SaveData(AdamData davaValues);
        private SaveData saver;
        public delegate void ErrorLog(string errorMsg);
        private ErrorLog errrorLogger;
        private AdamDataReadout typeToRead;
        private bool running;
        private Thread adamReader;
        private AdamData davaValues;

        private bool Connect()
        {
            bool connectionResult = false;
            lock (this.adamModbusLock)
            {
                connectionResult = this.adamModbus.Connect(IpAddress, ProtocolType.Tcp, Port);
            }
            if (!connectionResult)
                this.errrorLogger?.Invoke("Unable to connect to IP " + IpAddress);
            return connectionResult;
        }

        public void Start()
        {
            if (!this.running)
            {
                lock (this.adamModbusLock)
                {
                    this.adamModbus = new AdamSocket();
                    this.adamModbus.SetTimeout(ConnectTimeOut, SendTimeOut, ReceiveTimeOut);
                }
                this.running = true;
                this.adamReader = new Thread(ReadingData);
                this.adamReader.Start();
            }
        }

        public void Stop()
        {
            this.running = false;
            // adamTCP.Modbus().ForceSingleCoil(33, 1) //start counter
            //      adamTCP.Modbus().ForceSingleCoil(34 + (Maquinas(indice).entradas.AlTrama * 4), 1)// clear counter 4 - val 50
            //     adamTCP.Modbus().ForceSingleCoil(34 + (Maquinas(indice).entradas.AlUrdimbre * 4), 1) // clear counter 3 -val 46
            //   // nacitava 2,3,4 registre
            //adamTCP.Modbus.ReadInputStatus(1, 8, EstadoEntradas)
            //Codigo	Descripcion
            //1   Utok AlTrama
            //2   Osnova AlUrdimbre
            // register 0 - pasadas
            // register 3 - AlUrdimbre -Osnova
            //  register 4 - AlTrama -Utok
            // register 2 -FinPieza
            // status of the machine is based on pasadas and time - why not based on signal?
        }

        private void ReadingData()
        {
            while (this.running)
            {
                Sleep(DateTime.Now, DisconnectTimeOut);
                if (Connect())
                {
                    while (this.running)
                    {
                        DateTime loopStart = DateTime.Now;
                        if (saver != null)
                        {
                            if (!ReadCounters())
                                break;
                            if (!ReadDigitalInputs())
                                break;
                            if (!ReadDigitalOutputs())
                                break;
                            if (saver(this.davaValues))
                                if (!ClearRegisters())
                                    break;
                        }
                        Sleep(loopStart, ReadTimeOut);
                    }
                    lock (this.adamModbusLock)
                    {
                        this.adamModbus.Disconnect();
                    }
                }
            }
        }

        private bool ReadCounters()
        {
            if (ShouldRead(AdamDataReadout.ReadCounters))
            {
                int[] resultArray = null;
                lock (this.adamModbusLock)
                {
                    if (adamModbus.Modbus().ReadHoldingRegs(1, 16, out resultArray))
                    {
                        this.davaValues.DiCounters = resultArray;
                        return true;
                    }
                    else
                    {
                        this.errrorLogger?.Invoke("Unable to read input registers");
                    }
                }
                return false;
            }
            return true;
        }

        private bool ReadDigitalInputs()
        {
            if (ShouldRead(AdamDataReadout.ReadDiValues))
            {
                bool[] resultArray = null;
                lock (this.adamModbusLock)
                {
                    if (adamModbus.Modbus().ReadCoilStatus(1, 8, out resultArray))
                    {
                        this.davaValues.Di = resultArray;
                        return true;
                    }
                    else
                    {
                        this.errrorLogger?.Invoke("Unable to read digital input");
                    }
                }
                return false;
            }
            return true;
        }

        private bool ReadDigitalOutputs()
        {
            if (ShouldRead(AdamDataReadout.ReadDoValues))
            {
                bool[] resultArray = null;
                lock (this.adamModbusLock)
                {
                    if (adamModbus.Modbus().ReadCoilStatus(17, 8, out resultArray))
                    {
                        this.davaValues.Do = resultArray;
                        return true;
                    }
                    else
                    {
                        this.errrorLogger?.Invoke("Unable to read digital output");
                    }
                }
                return false;
            }
            return true;
        }

        private bool ClearRegisters()
        {
            if (ShouldRead(AdamDataReadout.ReadCounters))
            {
                int[] counterValues = this.davaValues.DiCounters;
                if (counterValues != null)
                {
                    int address = 34;
                    int bitVal = 0;
                    while (address <= 62)
                    {
                        if (ClearRegister(counterValues, bitVal, bitVal + 1, address))
                        {
                            bitVal += 2;
                            address += 4;
                        }
                        else
                            return false;
                    }
                }
            }
            return true;
        }

        private bool ClearRegister(int[] counterValues, int bitOne, int bitTwo, int address)
        {
            if (counterValues[bitOne] != 0 || counterValues[bitTwo] != 0)
            {
                lock (this.adamModbusLock)
                {
                    if (adamModbus.Modbus().ForceSingleCoil(address, 1))
                        return true;
                    else
                    {
                        this.errrorLogger?.Invoke("Unable to reset register, address " + address);
                        return false;
                    }
                }
            }
            else
                return true;
        }

        private bool ShouldRead(AdamDataReadout readType)
        {
            return ((int)this.typeToRead & (int)readType) != 0;
        }

        private int GetSleepTimeAmount(DateTime startTime, int loopRepeatTime)
        {
            TimeSpan difference = DateTime.Now - startTime;
            if (difference.TotalMilliseconds >= loopRepeatTime)
                return 0;
            else
                return loopRepeatTime - (int)difference.TotalMilliseconds;
        }

        private void Sleep(DateTime startTime, int loopRepeatTime)
        {
            while (GetSleepTimeAmount(startTime, loopRepeatTime) > 0 && this.running)
            {
                Thread.Sleep(StopSleepTimeOut);
            }
        }

        public AdamConnector(string ipAddress, int port, SaveData saver, ErrorLog logger)
        {
            IpAddress = ipAddress;
            ConnectTimeOut = DEFALUT_TIMOUT_VALUE;
            SendTimeOut = DEFALUT_TIMOUT_VALUE;
            ReceiveTimeOut = DEFALUT_TIMOUT_VALUE;
            ReadTimeOut = DEFALUT_TIMOUT_VALUE;
            DisconnectTimeOut = DEFALUT_TIMOUT_VALUE;
            Port = port;
            this.running = false;
            this.adamReader = null;
            this.davaValues = new AdamData();
            this.typeToRead = AdamDataReadout.None;
            this.saver = saver;
            this.errrorLogger = logger;
            this.adamModbusLock = new object();
            StopSleepTimeOut = 100;
        }

        public AdamConnector(string ipAddress, int port, int disconnectTimeOut, int readTimeOut, SaveData saver, ErrorLog logger, AdamDataReadout dataTypeToRead)
        {
            IpAddress = ipAddress;
            ConnectTimeOut = DEFALUT_TIMOUT_VALUE;
            SendTimeOut = DEFALUT_TIMOUT_VALUE;
            ReceiveTimeOut = DEFALUT_TIMOUT_VALUE;
            ReadTimeOut = readTimeOut;
            DisconnectTimeOut = disconnectTimeOut;
            Port = port;
            this.running = false;
            this.adamReader = null;
            this.davaValues = new AdamData();
            this.typeToRead = dataTypeToRead;
            this.saver = saver;
            this.errrorLogger = logger;
            this.adamModbusLock = new object();
            StopSleepTimeOut = 100;
        }
    }
}
