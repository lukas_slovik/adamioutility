﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamUtility
{
    public class AdamData
    {
        private int[] diCounters;
        private object diCountersLock;
        public int[] DiCounters
        {
            get
            {
                lock (this.diCountersLock)
                {
                    if (this.diCounters != null)
                    {
                        return (int[])this.diCounters.Clone();
                    }
                    return null;
                }
            }
            set
            {
                lock (this.diCountersLock)
                {
                    this.diCounters = value;
                }
            }
        }
        private object diLock;
        private bool[] diValues;
        public bool[] Di
        {
            get
            {
                lock (this.diLock)
                {
                    if (this.diValues != null)
                    {
                        return (bool[])this.diValues.Clone();
                    }
                    return null;
                }
            }
            set
            {
                lock (this.diLock)
                {
                    this.diValues = value;
                }
            }
        }
        private object doLock;
        private bool[] doValues;
        public bool[] Do
        {
            get
            {
                lock (this.doLock)
                {
                    if (this.doValues != null)
                    {
                        return (bool[])this.doValues.Clone();
                    }
                    return null;
                }
            }
            set
            {
                lock (this.doLock)
                {
                    this.doValues = value;
                }
            }
        }

        public AdamData()
        {
            this.diCountersLock = new object();
            this.diLock = new object();
            this.doLock = new object();
            DiCounters = null;
            Di = null;
            Do = null;
        }
    }
}
